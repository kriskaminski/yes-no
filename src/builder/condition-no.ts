/** Package information retrieved from `package.json` using webpack. */
declare const PACKAGE_NAME: string;
declare const PACKAGE_VERSION: string;

/** Dependencies */
import { ConditionBlock, pgettext, tripetto } from "tripetto";
import { YesNo } from "./";

/** Assets */
import ICON from "../../assets/no.svg";

@tripetto({
    type: "condition",
    identifier: `${PACKAGE_NAME}:no`,
    version: PACKAGE_VERSION,
    alias: "yes-no:no",
    get label() {
        return pgettext("block:yes-no", "No");
    },
    icon: ICON,
    context: YesNo,
})
export class No extends ConditionBlock {}
