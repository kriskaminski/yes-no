/** Package information retrieved from `package.json` using webpack. */
declare const PACKAGE_NAME: string;
declare const PACKAGE_VERSION: string;

import {
    Forms,
    NodeBlock,
    REGEX_IS_URL,
    Slots,
    conditions,
    definition,
    editor,
    isString,
    pgettext,
    slots,
    tripetto,
} from "tripetto";
import { IYesNo } from "../runner";
import { Yes } from "./condition-yes";
import { No } from "./condition-no";

/** Assets */
import ICON from "../../assets/icon.svg";

@tripetto({
    type: "node",
    identifier: PACKAGE_NAME,
    alias: "yes-no",
    version: PACKAGE_VERSION,
    get label() {
        return pgettext("block:yes-no", "Yes/No");
    },
    icon: ICON,
})
export class YesNo extends NodeBlock implements IYesNo {
    public answerSlot!: Slots.String;

    @definition
    imageURL?: string;

    @definition
    imageWidth?: string;

    @definition
    imageAboveText?: boolean;

    @definition
    altYes?: string;

    @definition
    altNo?: string;

    @slots
    defineSlot(): void {
        this.answerSlot = this.slots.static({
            type: Slots.String,
            reference: "answer",
            label: pgettext("block:yes-no", "Answer"),
        });
    }

    @editor
    defineEditor(): void {
        this.editor.name(true, true);
        this.editor.description();
        this.editor.option({
            name: pgettext("block:yes-no", "Image"),
            form: {
                title: pgettext("block:yes-no", "Image"),
                controls: [
                    new Forms.Text(
                        "singleline",
                        Forms.Text.bind(this, "imageURL", undefined)
                    )
                        .label(pgettext("block:yes-no", "Image source URL"))
                        .inputMode("url")
                        .placeholder("https://")
                        .autoValidate((ref: Forms.Text) =>
                            ref.value === ""
                                ? "unknown"
                                : REGEX_IS_URL.test(ref.value) ||
                                  (ref.value.length > 23 &&
                                      ref.value.indexOf(
                                          "data:image/jpeg;base64,"
                                      ) === 0) ||
                                  (ref.value.length > 22 &&
                                      ref.value.indexOf(
                                          "data:image/png;base64,"
                                      ) === 0) ||
                                  (ref.value.length > 22 &&
                                      ref.value.indexOf(
                                          "data:image/svg;base64,"
                                      ) === 0) ||
                                  (ref.value.length > 22 &&
                                      ref.value.indexOf(
                                          "data:image/gif;base64,"
                                      ) === 0) ||
                                  (ref.value.length > 1 &&
                                      ref.value.charAt(0) === "/")
                                ? "pass"
                                : "fail"
                        ),
                    new Forms.Text(
                        "singleline",
                        Forms.Checkbox.bind(this, "imageWidth", undefined)
                    )
                        .label(
                            pgettext("block:yes-no", "Image width (optional)")
                        )
                        .width(100)
                        .align("center"),
                    new Forms.Checkbox(
                        pgettext(
                            "block:yes-no",
                            "Display image on top of the paragraph"
                        ),
                        Forms.Checkbox.bind(this, "imageAboveText", undefined)
                    ),
                ],
            },
            activated: isString(this.imageURL),
        });
        this.editor.explanation();

        this.editor.groups.settings();
        this.editor.option({
            name: pgettext("block:yes-no", "Labels"),
            form: {
                title: pgettext("block:yes-no", "Labels"),
                controls: [
                    new Forms.Text(
                        "singleline",
                        Forms.Text.bind(this, "altYes", undefined)
                    ).placeholder(pgettext("block:yes-no", "Yes")),
                    new Forms.Text(
                        "singleline",
                        Forms.Text.bind(this, "altNo", undefined)
                    ).placeholder(pgettext("block:yes-no", "No")),
                ],
            },
            activated: isString(this.altYes) || isString(this.altNo),
        });

        this.editor.groups.options();
        this.editor.required(this.answerSlot);
        this.editor.visibility();
        this.editor.alias(this.answerSlot);
        this.editor.exportable(this.answerSlot);
    }

    @conditions
    defineConditions(): void {
        this.conditions.template({
            condition: Yes,
            label: Yes.label,
            props: {
                slot: this.answerSlot,
            },
        });

        this.conditions.template({
            condition: No,
            label: No.label,
            props: {
                slot: this.answerSlot,
            },
        });
    }
}
