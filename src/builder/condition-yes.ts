/** Package information retrieved from `package.json` using webpack. */
declare const PACKAGE_NAME: string;
declare const PACKAGE_VERSION: string;

/** Dependencies */
import { ConditionBlock, pgettext, tripetto } from "tripetto";
import { YesNo } from "./";

/** Assets */
import ICON from "../../assets/yes.svg";

@tripetto({
    type: "condition",
    identifier: `${PACKAGE_NAME}:yes`,
    version: PACKAGE_VERSION,
    alias: "yes-no:yes",
    get label() {
        return pgettext("block:yes-no", "Yes");
    },
    icon: ICON,
    context: YesNo,
})
export class Yes extends ConditionBlock {}
