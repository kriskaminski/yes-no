/** Package information retrieved from `package.json` using webpack. */
declare const PACKAGE_NAME: string;

import * as Tripetto from "tripetto-runner-foundation";

@Tripetto.block({
    type: "condition",
    identifier: `${PACKAGE_NAME}:no`,
    alias: "yes-no:no",
})
export class No extends Tripetto.ConditionBlock {
    @Tripetto.condition
    isNo(): boolean {
        const answerSlot = this.valueOf<string>();

        return answerSlot ? answerSlot.value === "no" : false;
    }
}
