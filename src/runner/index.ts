import { NodeBlock, assert } from "tripetto-runner-foundation";

import "./condition-no";
import "./condition-yes";

export interface IYesNo {
    readonly imageURL?: string;
    readonly imageWidth?: string;
    readonly imageAboveText?: boolean;
    readonly altYes?: string;
    readonly altNo?: string;
}

export abstract class YesNo extends NodeBlock<IYesNo> {
    readonly answerSlot = assert(this.valueOf<"" | "yes" | "no">("answer"));
    readonly required = this.answerSlot.slot.required || false;

    toggle(data: "yes" | "no"): void {
        this.answerSlot.value = this.answerSlot.value === data ? "" : data;
    }
}
