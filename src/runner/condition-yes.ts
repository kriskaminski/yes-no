/** Package information retrieved from `package.json` using webpack. */
declare const PACKAGE_NAME: string;

import * as Tripetto from "tripetto-runner-foundation";

@Tripetto.block({
    type: "condition",
    identifier: `${PACKAGE_NAME}:yes`,
    alias: "yes-no:yes",
})
export class Yes extends Tripetto.ConditionBlock {
    @Tripetto.condition
    isYes(): boolean {
        const answerSlot = this.valueOf<string>();

        return answerSlot ? answerSlot.value === "yes" : false;
    }
}
